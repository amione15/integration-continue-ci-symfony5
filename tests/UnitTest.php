<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Demo;


class UnitTest extends TestCase
{
    public function testDemo(): void
    {
        $demo = new Demo();
        $demo -> setDemo('test1');
        $this-> assertTrue($demo -> getDemo() === 'test1');
    }
}
