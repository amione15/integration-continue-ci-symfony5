<?php

namespace App\Tests;

// use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

// class FunctionalTest extends PantherTestCase
class PostControllerTest extends WebTestCase
{
    public function testShouldDisplayDemo(): void
    {
        // $client = static::createPantherClient();
        $client = static::createClient();
        $client -> followRedirects();
        $crawler = $client->request('GET', '/demo');

        // Validate a successful response and some content
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Demo index');
    }
}
