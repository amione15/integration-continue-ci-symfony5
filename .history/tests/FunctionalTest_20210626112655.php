<?php

namespace App\Tests;

// use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

// class FunctionalTest extends PantherTestCase
class PostControllerTest extends WebTestCase
{
    public function testShouldDisplayDemo(): void
    {
        // $client = static::createPantherClient();
        $client = static::createClient();
        $client -> followRedirects();
        $crawler = $client->request('GET', '/demo');

        // Validate a successful response and some content
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Demo index');
    }


    public function testShouldDisplayCreateNewDemo()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/demo/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Demo');
    }

    public function testShouldAddNewDemo()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/demo/new');
    
        // selectioner le bouton save
        $buttonCrawlerNode = $crawler->selectButton('Save');
    
        // récupérer le formulaire correspondant
        $form = $buttonCrawlerNode->form();
    
        $uuid = uniqid();
    
        $form = $buttonCrawlerNode->form([
            'demo[demo]'    => 'Add Demo For Test' . $uuid,
        ]);
        
        $client->submit($form);
        
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Add Demo For Test' . $uuid);
    }


}
