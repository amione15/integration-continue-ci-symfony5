<?php

namespace App\Tests;

// use Symfony\Component\Panther\PantherTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends PantherTestCase
class PostControllerTest extends WebTestCase
{
    public function testShouldDisplayDemo(): void
    {
        $client = static::createPantherClient();
        $client -> followRedirects();
        $crawler = $client->request('GET', '/demo');

        $this->assertSelectorTextContains('h1', 'Demo index');
    }
}
